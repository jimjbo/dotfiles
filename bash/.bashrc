if [[ $- != *i* ]] ; then
	return
fi

extra=""

if [[ -n $SSH_CLIENT ]]; then
	extra="[$HOSTNAME]"
fi

#bash prompt config
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ [\1]/'
}

export PS1="\[\033[38;5;239m\]\\\$$extra\[$(tput sgr0)\] \[\033[38;5;63m\]\u\[$(tput sgr0)\] \[\033[38;5;163m\]::\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;41m\]\W\[$(tput sgr0)\]\[\033[38;5;208m\]\$(parse_git_branch)\[$(tput sgr0)\]\[\033[00m\] >\[$(tput sgr0)\] "

##ALIASES

#abbreviated commands for xbps
alias pcs="sudo pacman -S"
alias pcu="sudo pacman -Syu"
alias pcr="sudo pacman -Rns"
alias pcq="pacman -Ss"

#alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

alias df="df -h"

alias ls="ls --color=auto"

alias clr="tput reset"
alias comp="picom -b -I 0.075 -O 0.075"

ex ()
{
    if [ -f "$1" ] ; then
        case $1 in
            *.tar.bz2)   tar xjf "$1"   ;;
            *.tar.gz)    tar xzf "$1"   ;;
            *.bz2)       bunzip2 "$1"   ;;
            *.rar)       unrar x "$1"   ;;
            *.gz)        gunzip "$1"    ;;
            *.tar)       tar xf "$1"    ;;
            *.tbz2)      tar xjf "$1"   ;;
            *.tgz)       tar xzf "$1"   ;;
            *.zip)       unzip "$1"     ;;
            *.Z)         uncompress "$1";;
            *.7z)        7z x "$1"      ;;
            *.deb)       ar x "$1"      ;;
            *.tar.xz)    tar xf "$1"    ;;
            *.tar.zst)   unzstd "$1"    ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

alias mirrorupdate="sudo reflector --verbose --latest 20 --protocol https --sort rate --save /etc/pacman.d/mirrorlist"
alias frefresh="fc-cache -f -v"

alias vpn-man="sudo openvpn /etc/openvpn/configs/uk-man.prod.surfshark.com_tcp.ovpn"
alias vpn-gla="sudo openvpn /etc/openvpn/configs/uk-gla.prod.surfshark.com_tcp.ovpn"

alias rtfm="man"
