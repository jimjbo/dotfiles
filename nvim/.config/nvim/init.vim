call plug#begin('~/.local/share/nvim/plugged')

"visual plugins
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"syntax highlighting
Plug 'sheerun/vim-polyglot'
Plug 'dense-analysis/ale'

"qol changes
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdcommenter'

"git functionality
Plug 'airblade/vim-gitgutter'

call plug#end()

"airline bar
let g:airline_theme='minimalist'
let g:airline_powerline_fonts=0

"custom colourscheme
"syntax on
"colorscheme bonedark
"set termguicolors
"set t_Co=256
"hi Normal guibg=NONE
"hi SignColumn guibg=NONE
"hi CursorLineNr guibg=NONE

colorscheme etc


"line numbering
set nu rnu

filetype plugin on

"tab formatting
set expandtab
set tabstop=8
set softtabstop=0
set shiftwidth=4
set smarttab

"solved issue with backup and writing to files
set backupdir=~/.config/nvim/tmp/backup
