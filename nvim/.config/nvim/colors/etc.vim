
" --------------------------------
set background=dark
" --------------------------------

highlight clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="etc"

set t_Co=256

"----------------------------------------------------------------
" General settings                                              |
"----------------------------------------------------------------
"----------------------------------------------------------------
" Syntax group   | Foreground    | Background    | Style        |
"----------------------------------------------------------------

" --------------------------------
" Editor settings
" --------------------------------
hi Normal          ctermfg=none    ctermbg=none    cterm=none
hi Cursor          ctermfg=none    ctermbg=none    cterm=none
hi CursorLine      ctermfg=none    ctermbg=none    cterm=none
hi LineNr          ctermfg=242    ctermbg=none    cterm=none
hi CursorLineNR    ctermfg=75    ctermbg=none    cterm=none

" -----------------
" - Number column -
" -----------------
hi CursorColumn    ctermfg=none    ctermbg=none    cterm=none
hi FoldColumn      ctermfg=none    ctermbg=none    cterm=none
hi SignColumn      ctermfg=none    ctermbg=none    cterm=none
hi Folded          ctermfg=242    ctermbg=none    cterm=none

" -------------------------
" - Window/Tab delimiters -
" -------------------------
hi VertSplit       ctermfg=none    ctermbg=none    cterm=none
hi ColorColumn     ctermfg=none    ctermbg=none    cterm=none
hi TabLine         ctermfg=none    ctermbg=none    cterm=none
hi TabLineFill     ctermfg=none    ctermbg=none    cterm=none
hi TabLineSel      ctermfg=none    ctermbg=none    cterm=none

" -------------------------------
" - File Navigation / Searching -
" -------------------------------
hi Directory       ctermfg=none    ctermbg=none    cterm=none
hi Search          ctermfg=233    ctermbg=222    cterm=none
hi IncSearch       ctermfg=none    ctermbg=none    cterm=none

" -----------------
" - Prompt/Status -
" -----------------
hi StatusLine      ctermfg=none    ctermbg=none    cterm=none
hi StatusLineNC    ctermfg=none    ctermbg=none    cterm=none
hi WildMenu        ctermfg=none    ctermbg=none    cterm=none
hi Question        ctermfg=none    ctermbg=none    cterm=none
hi Title           ctermfg=none    ctermbg=none    cterm=none
hi ModeMsg         ctermfg=none    ctermbg=none    cterm=none
hi MoreMsg         ctermfg=none    ctermbg=none    cterm=none

" --------------
" - Visual aid -
" --------------
hi MatchParen      ctermfg=none    ctermbg=none    cterm=none
hi Visual          ctermfg=none    ctermbg=none    cterm=none
hi VisualNOS       ctermfg=none    ctermbg=none    cterm=none
hi NonText         ctermfg=none    ctermbg=none    cterm=none

hi Todo            ctermfg=none    ctermbg=none    cterm=none
hi Underlined      ctermfg=none    ctermbg=none    cterm=none
hi Error           ctermfg=168    ctermbg=none    cterm=none
hi ErrorMsg        ctermfg=168    ctermbg=none    cterm=none
hi WarningMsg      ctermfg=179    ctermbg=none    cterm=none
hi Ignore          ctermfg=none    ctermbg=none    cterm=none
hi SpecialKey      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Variable types
" --------------------------------
hi Constant        ctermfg=173    ctermbg=none    cterm=none
hi String          ctermfg=150    ctermbg=none    cterm=none
hi StringDelimiter ctermfg=150    ctermbg=none    cterm=none
hi Character       ctermfg=150    ctermbg=none    cterm=none
hi Number          ctermfg=173    ctermbg=none    cterm=none
hi Boolean         ctermfg=173    ctermbg=none    cterm=none
hi Float           ctermfg=173    ctermbg=none    cterm=none

hi Identifier      ctermfg=73    ctermbg=none    cterm=none
hi Function        ctermfg=69    ctermbg=none    cterm=none

" --------------------------------
" Language constructs
" --------------------------------
hi Statement       ctermfg=134    ctermbg=none    cterm=none
hi Conditional     ctermfg=134    ctermbg=none    cterm=none
hi Repeat          ctermfg=179    ctermbg=none    cterm=none
hi Label           ctermfg=179    ctermbg=none    cterm=none
hi Operator        ctermfg=134    ctermbg=none    cterm=none
hi Keyword         ctermfg=134    ctermbg=none    cterm=none
hi Exception       ctermfg=69    ctermbg=none    cterm=none
hi Comment         ctermfg=242    ctermbg=none    cterm=none

hi Special         ctermfg=210    ctermbg=none    cterm=none
hi SpecialChar     ctermfg=210    ctermbg=none    cterm=none
hi Tag             ctermfg=179    ctermbg=none    cterm=none
hi Delimiter       ctermfg=250    ctermbg=none    cterm=none
hi SpecialComment  ctermfg=242    ctermbg=none    cterm=none
hi Debug           ctermfg=69    ctermbg=none    cterm=none

" ----------
" - C like -
" ----------
hi PreProc         ctermfg=179    ctermbg=none    cterm=none
hi Include         ctermfg=69    ctermbg=none    cterm=none
hi Define          ctermfg=134    ctermbg=none    cterm=none
hi Macro           ctermfg=146    ctermbg=none    cterm=none
hi PreCondit       ctermfg=134    ctermbg=none    cterm=none

hi Type            ctermfg=36      ctermbg=none    cterm=none
hi StorageClass    ctermfg=179    ctermbg=none    cterm=none
hi Structure       ctermfg=134    ctermbg=none    cterm=none
hi Typedef         ctermfg=36    ctermbg=none    cterm=none

" --------------------------------
" Diff
" --------------------------------
hi DiffAdd         ctermfg=150    ctermbg=none    cterm=none
hi DiffChange      ctermfg=222    ctermbg=none    cterm=none
hi DiffDelete      ctermfg=168    ctermbg=none    cterm=none
hi DiffText        ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Completion menu
" --------------------------------
hi Pmenu           ctermfg=none    ctermbg=none    cterm=none
hi PmenuSel        ctermfg=none    ctermbg=29    cterm=none
hi PmenuSbar       ctermfg=none    ctermbg=none    cterm=none
hi PmenuThumb      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Spelling
" --------------------------------
hi SpellBad        ctermfg=none    ctermbg=none    cterm=none
hi SpellCap        ctermfg=none    ctermbg=none    cterm=none
hi SpellLocal      ctermfg=none    ctermbg=none    cterm=none
hi SpellRare       ctermfg=none    ctermbg=none    cterm=none

